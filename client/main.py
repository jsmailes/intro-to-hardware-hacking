import config

import json
import requests
import time

import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)
GPIO.setup(8, GPIO.OUT)

def make_request(command, data):
    data_dump = json.dumps(data)
    r = requests.get("http://{}/{}".format(config.server_host, command), params=dict(data=data_dump))
    return r.json()

while True:
    r = make_request("synchronise", dict())
    led_status = r.get("led_status")
    if led_status is not None:
        if led_status:
            GPIO.output(8, True)
        else:
            GPIO.output(8, False)
    time.sleep(0.5)
