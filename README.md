# Intro to Hardware Hacking

Repository for the "Introduction to Hardware Hacking" workshop at Oxford Hack 2019.

This workshop walks you through setting up a [Raspberry Pi](https://www.raspberrypi.org/) to interface with a remote web server for the purpose of controlling the Pi's GPIO remotely. This will allow you to create all kinds of IoT devices!

We are synchronising data between the web server and the Pi by having the Pi regularly request updates from the server.

## Getting Started

### Server

1. Set up a remote server instance on [DigitalOcean](https://www.digitalocean.com/), [Azure](https://azure.microsoft.com/), [AWS](https://aws.amazon.com/), etc. For the sake of consistency with this tutorial, set up the server to run Ubuntu - everything in this tutorial is possible on other distros, but the package manager commands will be different.
2. Use `ssh` to connect to the server.
3. Create a new user (TODO possibly remove this step):  
```
adduser USERNAME
usermod -aG sudo USERNAME
su - USERNAME
```
3. Clone this repository into the home folder:  
```
git clone https://gitlab.com/jsmailes/intro-to-hardware-hacking.git
cd intro-to-hardware-hacking/server
```
4. Install the required packages:
```
sudo apt update
sudo apt install python3-pip
pip3 install --user cherrypy
```
5. Edit the `config.py` file so the `host` variable is the IP address of your server.
6. Set up the firewall:  
```
sudo ufw allow in from any to any port 80
```
7. Run the server:  
```
sudo python3 main.py
```
8. Navigate to the server in your browser to check that it worked.

### Client

The client side is fairly simple, we just need to set up scripts to pull data from the server at fixed time intervals.

1. Set up a raspberry pi so that you can `ssh` to it or use a keyboard and monitor. The default Raspbian installation should have most tools you need.
2. Install the required packages:  
```
sudo apt install git rpi.gpio
```
3. Clone the repository into the home folder:  
```
git clone https://gitlab.com/jsmailes/intro-to-hardware-hacking.git
cd intro-to-hardware-hacking/client
```
4. Connect an LED to pin 24 (BCM 8) of the Pi.
5. Modify `config.py` to point to the correct server address.
6. Run the script:  
```
python3 main.py
```

## Extending the project

You can use the GPIO pins on the Raspberry Pi to implement all kinds of circuitry - see what you can come up with!

If you want to send more data (either from the server to the pi or vice versa) simply modify the `synchronise` command in the server's `main.py` and add the extra data to the client's `main.py`.

If you want to make the site prettier, modify `server/index.html`!
