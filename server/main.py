import os
import json

import config

import cherrypy

cwd = os.getcwd()

class Server(object):
    def __init__(self):
        self.version = config.version
        self.led_status = False

    @cherrypy.expose
    def request_led(self):
        self.led_status = not self.led_status

    @cherrypy.expose
    def synchronise(self, data):
        data_json = json.loads(data)
        if data_json.get("led_status") is not None:
            self.led_status = data_json.get("led_status")
        output = dict(led_status=self.led_status)
        return json.dumps(output)

    @cherrypy.expose
    def version(self):
        return self.version

    @cherrypy.expose
    def index(self):
        return cherrypy.lib.static.serve_file(cwd + "/index.html", content_type="text/html")

if __name__ == '__main__':
    conf = {
        '/': {
            'tools.sessions.on': True,
            'tools.response_headers.on': True,
            'tools.response_headers.headers': [('Access-Control-Allow-Origin', '*')],
        },
        '/main.css': {
            'tools.staticfile.on': True,
            'tools.staticfile.filename': cwd + "/main.css"
        },
        '/main.js': {
            'tools.staticfile.on': True,
            'tools.staticfile.filename': cwd + "/main.js"
        },
    }

    cherrypy.config.update({'server.socket_host': config.host,
                            'server.socket_port': 80,
                          })

    cherrypy.quickstart(Server(), '/', conf)
